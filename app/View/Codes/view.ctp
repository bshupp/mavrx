<div class="codes view">
<h2><?php echo __('Code'); ?></h2>
	<dl>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo $this->Html->link($code['Code']['code_name'], array('controller' => 'codes', 'action' => 'view', $code['Code']['code_id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($code['CodeGroup']['code_group_id'], array('controller' => 'code_groups', 'action' => 'view', $code['CodeGroup']['code_group_id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Name'); ?></dt>
		<dd>
			<?php echo h($code['Code']['code_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Description'); ?></dt>
		<dd>
			<?php echo h($code['Code']['code_description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Code'), array('action' => 'edit', $code['Code']['code_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Code'), array('action' => 'delete', $code['Code']['code_id']), array(), __('Are you sure you want to delete # %s?', $code['Code']['code_id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Codes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Codes'), array('controller' => 'codes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code'), array('controller' => 'codes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Code Groups'), array('controller' => 'code_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code Group'), array('controller' => 'code_groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Codes'); ?></h3>
	<?php if (!empty($code['Code'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Code Id'); ?></th>
		<th><?php echo __('Code Group Id'); ?></th>
		<th><?php echo __('Code Name'); ?></th>
		<th><?php echo __('Code Description'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($code['Code'] as $code): ?>
		<tr>
			<td><?php echo $code['code_id']; ?></td>
			<td><?php echo $code['code_group_id']; ?></td>
			<td><?php echo $code['code_name']; ?></td>
			<td><?php echo $code['code_description']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'codes', 'action' => 'view', $code['code_id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'codes', 'action' => 'edit', $code['code_id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'codes', 'action' => 'delete', $code['code_id']), array(), __('Are you sure you want to delete # %s?', $code['code_id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Code'), array('controller' => 'codes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
