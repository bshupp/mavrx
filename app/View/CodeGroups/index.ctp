<div class="codeGroups index">
	<h2><?php echo __('Code Groups'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('code_group_id'); ?></th>
			<th><?php echo $this->Paginator->sort('code_group_name'); ?></th>
			<th><?php echo $this->Paginator->sort('code_group_description'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($codeGroups as $codeGroup): ?>
	<tr>
		<td><?php echo h($codeGroup['CodeGroup']['code_group_id']); ?>&nbsp;</td>
		<td><?php echo h($codeGroup['CodeGroup']['code_group_name']); ?>&nbsp;</td>
		<td><?php echo h($codeGroup['CodeGroup']['code_group_description']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $codeGroup['CodeGroup']['code_group_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $codeGroup['CodeGroup']['code_group_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $codeGroup['CodeGroup']['code_group_id']), array('confirm' => __('Are you sure you want to delete # %s?', $codeGroup['CodeGroup']['code_group_id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Code Group'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Codes'), array('controller' => 'codes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code'), array('controller' => 'codes', 'action' => 'add')); ?> </li>
	</ul>
</div>
