<div class="codeGroups form">
<?php echo $this->Form->create('CodeGroup'); ?>
	<fieldset>
		<legend><?php echo __('Edit Code Group'); ?></legend>
	<?php
		echo $this->Form->input('code_group_id');
		echo $this->Form->input('code_group_name');
		echo $this->Form->input('code_group_description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CodeGroup.code_group_id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('CodeGroup.code_group_id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Code Groups'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Codes'), array('controller' => 'codes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code'), array('controller' => 'codes', 'action' => 'add')); ?> </li>
	</ul>
</div>
