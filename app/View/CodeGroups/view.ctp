<div class="codeGroups view">
<h2><?php echo __('Code Group'); ?></h2>
	<dl>
		<dt><?php echo __('Code Group Id'); ?></dt>
		<dd>
			<?php echo h($codeGroup['CodeGroup']['code_group_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Group Name'); ?></dt>
		<dd>
			<?php echo h($codeGroup['CodeGroup']['code_group_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Group Description'); ?></dt>
		<dd>
			<?php echo h($codeGroup['CodeGroup']['code_group_description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Code Group'), array('action' => 'edit', $codeGroup['CodeGroup']['code_group_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Code Group'), array('action' => 'delete', $codeGroup['CodeGroup']['code_group_id']), array(), __('Are you sure you want to delete # %s?', $codeGroup['CodeGroup']['code_group_id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Code Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code Group'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Codes'), array('controller' => 'codes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Code'), array('controller' => 'codes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Codes'); ?></h3>
	<?php if (!empty($codeGroup['Code'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Code Id'); ?></th>
		<th><?php echo __('Code Group Id'); ?></th>
		<th><?php echo __('Code Name'); ?></th>
		<th><?php echo __('Code Description'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($codeGroup['Code'] as $code): ?>
		<tr>
			<td><?php echo $code['code_id']; ?></td>
			<td><?php echo $code['code_group_id']; ?></td>
			<td><?php echo $code['code_name']; ?></td>
			<td><?php echo $code['code_description']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'codes', 'action' => 'view', $code['code_id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'codes', 'action' => 'edit', $code['code_id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'codes', 'action' => 'delete', $code['code_id']), array(), __('Are you sure you want to delete # %s?', $code['code_id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Code'), array('controller' => 'codes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
