<?php
/**
 * CodeFixture
 *
 */
class CodeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'code';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'code_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'code_group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'code_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'code_description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'code_id', 'unique' => 1),
			'code_group_id' => array('column' => 'code_group_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'code_id' => 1,
			'code_group_id' => 1,
			'code_name' => 'Lorem ip',
			'code_description' => 'Lorem ipsum dolor '
		),
	);

}
