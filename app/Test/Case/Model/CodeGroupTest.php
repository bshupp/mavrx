<?php
App::uses('CodeGroup', 'Model');

/**
 * CodeGroup Test Case
 *
 */
class CodeGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.code_group',
		'app.code'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CodeGroup = ClassRegistry::init('CodeGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CodeGroup);

		parent::tearDown();
	}

}
