<?php
App::uses('AppController', 'Controller');
/**
 * CodeGroups Controller
 *
 * @property CodeGroup $CodeGroup
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CodeGroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CodeGroup->recursive = 0;
		$this->set('codeGroups', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CodeGroup->exists($id)) {
			throw new NotFoundException(__('Invalid code group'));
		}
		$options = array('conditions' => array('CodeGroup.' . $this->CodeGroup->primaryKey => $id));
		$this->set('codeGroup', $this->CodeGroup->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CodeGroup->create();
			if ($this->CodeGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The code group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The code group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CodeGroup->exists($id)) {
			throw new NotFoundException(__('Invalid code group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CodeGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The code group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The code group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CodeGroup.' . $this->CodeGroup->primaryKey => $id));
			$this->request->data = $this->CodeGroup->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CodeGroup->id = $id;
		if (!$this->CodeGroup->exists()) {
			throw new NotFoundException(__('Invalid code group'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CodeGroup->delete()) {
			$this->Session->setFlash(__('The code group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The code group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
