<?php
App::uses('AppModel', 'Model');
/**
 * Code Model
 *
 * @property CodeGroup $CodeGroup
 */
class Code extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'code';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'code_id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'code_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'code_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code_group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CodeGroup' => array(
			'className' => 'CodeGroup',
			'foreignKey' => 'code_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
